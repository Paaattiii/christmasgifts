package de.dieunkreativen.christmasgifts;

import java.util.logging.Logger;

import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;


public class ChristmasGifts extends JavaPlugin {
	Logger log;
	PluginDescriptionFile pdfFile = this.getDescription();
	static ChristmasGifts plugin;
	private final InteractListener interactListener = new InteractListener(this);
	public final ConfigHandler cfg = new ConfigHandler(this);
	
    @Override
    public void onLoad() {
            log = getLogger();
    }
    
	@Override
	public void onEnable() {
		plugin = this;
		PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(interactListener, this);
        this.getCommand("christmas").setExecutor(new Executor(this));
        cfg.loadConfig();
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is enabled!");
	}
	
	@Override
	public void onDisable() {
		log.info(pdfFile.getName() + " version " + pdfFile.getVersion() + " is disabled!");
	}
	
	public static ChristmasGifts getSelf() {
		return plugin;
	}

}
