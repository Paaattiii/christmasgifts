package de.dieunkreativen.christmasgifts;

import java.util.HashSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class InteractListener implements Listener {
	
	ChristmasGifts plugin;
	
	public InteractListener(ChristmasGifts instance) {
		plugin = instance;
	}
	
    @EventHandler
    public void onChestInteract(PlayerInteractEvent event) {
        Block b = event.getClickedBlock();
        Player p = event.getPlayer();
        
        
        if (b != null && b.getType() == Material.CHEST && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
        	if(Executor.settingChest.containsKey(p)) {
        		plugin.cfg.addChest(b.getLocation(), Executor.settingChest.get(p));
        		Executor.settingChest.remove(p);
        		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &aTruhe wurde erfolgreich hinzugef�gt!"));
        		event.setCancelled(true);
        		return;
        	}
        	

        	if (p.getItemInHand().getType() == Material.BLAZE_ROD && p.isOp()) {
        		return;
        	}
        	
        	
        	//auf normale Kiste geklickt -> nichts unternehmen
        	if(plugin.cfg.isValidChristmasChest(b.getLocation()) == false) {
        		return;
        	}
        	
        	//auf Kiste von Adventskalender geklickt, aber nicht der richtige Tag -> abbruch
        	if(plugin.cfg.isTodaysChristmasChest(b.getLocation()) == false) {
        		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &6Du darfst nur das heutige T�rchen �ffnen!"));
        		event.setCancelled(true);
        		return;
        	}
        	
        	//Ist Inventar voll? -> abbruch
        	if(isInventoryFull(p) == true) {
        		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &6Dein Inventar ist voll!"));
        		event.setCancelled(true);
        		return;
        	}
        	
        	
        	
        	if(plugin.cfg.canReceive(p) == true) {
        		Chest chest = (Chest) b.getState();
        		Inventory chestInv = chest.getInventory();
        		openInventory(chestInv, p);
        		p.playSound(p.getLocation(), Sound.LEVEL_UP, 1F, 0);
        		plugin.cfg.addPlayer(p, plugin.cfg.getCurrentDay());
        	}
        	else {
        		p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &6Du hast dieses T�rchen bereits ge�ffnet!"));
        	}
        	event.setCancelled(true);
        }
    }
    
    
	@EventHandler
	public void onPlayerJoin(final PlayerJoinEvent event) {
		final Player p = event.getPlayer();
		Bukkit.getScheduler().runTaskLater(ChristmasGifts.getSelf(), new Runnable() {
			  public void run() {
				  if(plugin.cfg.canReceive(p))
					p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &aVergiss heute nicht, das T�rchen vom Adventskalender zu �ffnen :)"));
			  }
			}, 80L);
	}
    
    public void openInventory(Inventory chestInv, Player p) {
    	Inventory custominv = Bukkit.createInventory(null, 27, "Adventskalender");
        HashSet<ItemStack> items = new HashSet<ItemStack>();
        
        for(ItemStack is : chestInv.getContents()){
            if(is == null)
                continue;
         
            items.add(is);
        }
     
        for(ItemStack is : items){
      	  custominv.addItem(is);
        }
  	  
  	  
  	  
  	  p.openInventory(custominv);
    }
    
    public boolean isInventoryFull(Player p) {
    	if (p.getInventory().firstEmpty() == -1){
    		return true;
    	}
    	return false;
    }
}
