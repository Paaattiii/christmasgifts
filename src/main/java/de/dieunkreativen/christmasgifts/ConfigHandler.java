package de.dieunkreativen.christmasgifts;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class ConfigHandler {
	
	ChristmasGifts plugin;
	
	public ConfigHandler(ChristmasGifts instance) {
		plugin = instance;
	}
	
	//Players.yml
	FileConfiguration configp;
	File filep;
	
	//Chests.yml
	FileConfiguration configc;
	File filec;

	public void loadConfig() {
		filep = new File(ChristmasGifts.getSelf().getDataFolder()+File.separator+"Players.yml");
		configp = YamlConfiguration.loadConfiguration(filep);
		
		filec = new File(ChristmasGifts.getSelf().getDataFolder()+File.separator+"Chests.yml");
		configc = YamlConfiguration.loadConfiguration(filec);
	}
	
	public void savePlayerConfig() {
		try {
			configp.save(filep);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void saveChestConfig() {
		try {
			configc.save(filec);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//Spieler hat ein T�rchen ge�ffnet und soll es nicht ein weiteres mal tun
	public void addPlayer(Player p, String day) {
		List<String> list = configp.getStringList(String.valueOf(day));
		list.add(p.getUniqueId().toString());
		configp.set(day, list);
		savePlayerConfig();
	}
	
	public void addChest(Location loc, String day) {
		configc.set(day + ".world", loc.getWorld().getName());
		configc.set(day + ".x", loc.getX());
		configc.set(day + ".y", loc.getY());
		configc.set(day + ".z", loc.getZ());
		saveChestConfig();
	}
	
	public boolean isTodaysChristmasChest(Location loc) {
		String w = configc.getString(getCurrentDay() + ".world");
		int x = configc.getInt(getCurrentDay() + ".x");
		int y = configc.getInt(getCurrentDay() + ".y");
		int z = configc.getInt(getCurrentDay() + ".z");
		
		if(loc.getWorld().getName().equalsIgnoreCase(w) && loc.getX() == x && loc.getY() == y && loc.getZ() == z) {
			return true;
		}
		return false;
	}
	
	public boolean isValidChristmasChest(Location loc) {
		for(int i=1; i<25; i++)
		{
			String w = configc.getString(i + ".world");
			int x = configc.getInt(i + ".x");
			int y = configc.getInt(i + ".y");
			int z = configc.getInt(i + ".z");
			
			if (configc.getString(i + ".world") == null) {
				continue;
	    	}
			
			if(loc.getWorld().getName().equalsIgnoreCase(w) && loc.getX() == x && loc.getY() == y && loc.getZ() == z) {
				return true;
			}
		}
		return false;
	}
	
	public boolean canReceive(Player p) {
		List<String> list = configp.getStringList(getCurrentDay());
		if (!list.contains(p.getUniqueId().toString())) {
			return true;
		}
		return false;
	}
	
	public String getCurrentDay() {
		Date d = new Date();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("d");
		return simpleDateFormat.format(d).toString();
		
		
	}
}
