package de.dieunkreativen.christmasgifts;

import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Executor  implements CommandExecutor{
	ChristmasGifts plugin;
	public static HashMap<Player, String> settingChest = new HashMap<Player, String>();
	
	public Executor(ChristmasGifts instance) {
		plugin = instance;
	}

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("christmas")) {
			if(sender instanceof Player) {
				Player p = (Player) sender;
				if(args.length > 0) {
					if(args[0].equalsIgnoreCase("set") && p.isOp()) {
						if(args.length == 2) {
							String door = args[1];
							settingChest.put(p, door);
							p.sendMessage(ChatColor.translateAlternateColorCodes('&', "&e[&4Adventskalender&e] &aNun Rechtsklick auf die Truhe bitte!"));
						}
					}
				}
			}
			return true;
		}
		return false;
	}

}
